let collection = [];

// Write the queue functions below.


function print() {
  return collection;
}

function enqueue(element){
	collection.push(element)
	return collection
}

function dequeue(element){
	collection.shift()
	return collection
}

function front(element){
	let first = collection.slice(0,1).toString()
	return first
}

function size(element){
	let size = collection.length
	return size
}

function isEmpty(element){
	return collection.length === 0
}


module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};